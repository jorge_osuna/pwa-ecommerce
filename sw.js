const CACHE_NAME = 'my-site-cache-v1';
const CACHE_DINAMIC = 'cache-dinamic';
const CACHE_INMUTABLE = 'cache-inmutable';

self.addEventListener('install',function(event){
    //Perform install steps
      const cacheAppShell = caches.open(CACHE_NAME).then(caches => {
            console.log('Opened cache');
            self.skipWaiting();
            return caches.addAll([
              './',
              './index.html',
              './css/style.css',
              './js/app.js',
              './img/cp.jpg',
              './img/discoduro.jpg',
              './img/gabinete.jpg',
              './img/memoria-ram.jpg',
              './manifest.webmanifest',
            ]);
        }).catch(error=>{
          console.log('Error Generar Cache name, ',error);
        });

      const cacheInmutable = caches.open(CACHE_INMUTABLE).then(cache=>{
        return cache.addAll([
          'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css',
          'https://kit.fontawesome.com/b27f078505.js',
          'https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js',
          'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js',
        ]);
      }).catch(error=>{
        console.log('Error al Generar Cache inmutable, ',error);
      });
      event.waitUntil(cacheAppShell);
});

//function poara borrar cache  recursivamente en promesas
function borrarCache() {
  caches.open(CACHE_DINAMIC).then(
      cache => {
          cache.keys().then(keys => {
              console.log("Numero de lementos en cache dinamico:" + keys.length);
              //limitar a 3 elementos
              if (keys.length > 3) {
                  cache.delete(keys[keys.length - 1]).then(
                      borrarCache()
                  );
              }
          });
      }
  )
}

self.addEventListener('activate', event => {
  console.log('El SW se ha activado.');
  Notification.requestPermission();
  event.waitUntil(borrarCache());

});

self.addEventListener('fetch', event => {
  console.log(event.request.url);
  //estrategia de gestion del cache - Cache Only
  // event.respondWith(
  //    caches.match(event.request)
  // );

  //estrategia del cache - Cache y despues Red
  event.respondWith(caches.match(event.request)
      .then(res => {
          //si la resp existe en el cache, regresar
          if (res) return res;
          else {
              console.log('No se encontró en el cache el ', event.request.url);
              //si la resp no existe
              //traerla de el sitio o el origen
              fetch(event.request).then(nuevoElemento => {
                  caches.open(CACHE_DINAMIC).then(cache => {
                      cache.put(event.request, nuevoElemento);
                  });
                  return nuevoElemento.clone();
              }).
              catch(error => {
                  console.log("error en fetch de estrategia de cache ", event.request.url, error);
              });


          }
      })
      .catch(error => {
          console.log('Error en fetch, ', error);
      })
  );


});
